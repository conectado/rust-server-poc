# Rust server POC

## Description

This is a POC to test Rust current capabilities as a web-server

We have chosen to use
* Actix (as a webserver framework)
* Diesel (as an ORM library)

We have chosen Actix due to its ability to work on stable and its simplicity and it seemingly fast response times

We have chosen Diesel to easily abstract from the database as it seems popular for this job

## Run this project

```
cargo install
cargo run
```

## Run with auto-reload

`systemfd --no-pid -s http::3000 -- cargo watch -x run`
